package com.bhm.user_management.auth.common;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 1/26/18
 */
public class AuthConstant {
    /**
     * JWT Property
     */
    static final public String SECRET = "R94Lq!88h75";
    static final public String TOKEN_PREFIX = "Bearer";
    static final public String HEADER_STRING = "Authorization";

    /**
     * URL
     */
    static private String AUTH_URL_PREFIX = "/bhm/auth";
    static final public String SIGN_UP_URL = AUTH_URL_PREFIX+"/signup";
    static final public String LOGIN_URL = AUTH_URL_PREFIX+"/login";

    /**
     * Message Constant
     */
    static final public String SUCCESS = " SUCCEED";
    static final public String FAIL = " FAILED";
    static final public String ACTIVE = " ACTIVE";
    static final public String USER = " USER";

}
