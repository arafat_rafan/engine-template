package com.bhm.user_management.auth.controller;

import com.bhm.common.BHMResponseMessage;
import com.bhm.common.ResponseMessage;
import com.bhm.user_management.auth.jwt.service.TokenAuthenticationService;
import com.bhm.user_management.auth.model.AuthUser;
import com.bhm.user_management.auth.model.UserCredential;
import com.bhm.user_management.auth.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 7/17/17
 */
@RestController
@RequestMapping("/bhm/auth/")
public class AuthController {

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);
    @Autowired
    private AuthService authService;
    @Autowired
    TokenAuthenticationService tokenAuthenticationService;

    @PostMapping("login")
    public ResponseMessage login(HttpServletRequest request, HttpServletResponse response, @RequestBody AuthUser authUser){
        logger.info("login "+authUser.toString());
        logger.info("login request "+request.getHeader("Content-Type"));
        ResponseMessage responseMessage = authService.login(authUser).join();
        if(responseMessage.isSuccess()){
            tokenAuthenticationService.addAuthentication(response, authUser.getUserName());
        }
        return responseMessage;
    }

    @PostMapping("signup")
    public ResponseMessage signup(HttpServletRequest request, @RequestBody AuthUser authUser){
        logger.info("signup "+authUser.toString());
        logger.info("signup request "+request.getHeader("Content-Type"));
        ResponseMessage responseMessage = null;
        if(!authUser.getUserName().isEmpty()){
            CompletableFuture<ResponseMessage> completableFuture = authService.createUser(authUser);
            try {
                responseMessage  = completableFuture.get();
                logger.info("signup bhmResponseMessage "+responseMessage.toString());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        return responseMessage;
    }
}
