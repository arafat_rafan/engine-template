package com.bhm.common;

import com.bhm.user_management.auth.model.AuthUser;

import java.util.List;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 1/30/18
 */
public class BHMResponseMessage implements ResponseMessage {
    private boolean isSuccess;
    private String responseMessage = null;
    private String rootCause = null;
    private List<AuthUser> data = null;

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public void setRootCause(String rootCause) {
        this.rootCause = rootCause;
    }

    @Override
    public List<AuthUser> getData() {
        return data;
    }

    public void setData(List<AuthUser> data) {
        this.data = data;
    }

    @Override
    public boolean isSuccess() {
        return isSuccess;
    }

    @Override
    public String getResponseMessage() {
        return responseMessage;
    }

    @Override
    public String getRootCause() {
        return rootCause;
    }

    @Override
    public String toString() {
        return "BHMResponseMessage{" +
                "isSuccess=" + isSuccess +
                ", responseMessage='" + responseMessage + '\'' +
                ", rootCause='" + rootCause + '\'' +
                '}';
    }
}
