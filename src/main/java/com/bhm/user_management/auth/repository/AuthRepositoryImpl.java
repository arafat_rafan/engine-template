package com.bhm.user_management.auth.repository;

import com.bhm.common.BHMResponseMessage;
import com.bhm.user_management.auth.model.AuthUser;
import com.bhm.user_management.user.model.User;
import com.mongodb.WriteResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import static com.bhm.user_management.auth.common.AuthConstant.SUCCESS;
import static com.bhm.user_management.auth.common.AuthConstant.FAIL;

/**
 * @author : arafat
 * @version : 1.0v.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 7/17/17
 */

@Repository
public class AuthRepositoryImpl implements AuthRepositoryCustom {

    private final Logger logger = LoggerFactory.getLogger(AuthRepositoryImpl.class);

    @Autowired
    MongoTemplate mongoTemplate;

    BHMResponseMessage bhmResponseMessage;

    private void getBhmResponseMessage(){
        bhmResponseMessage =  new BHMResponseMessage();
    }

    @Override
    public BHMResponseMessage updatePassword(String userName, String password, String collectionName) {
        getBhmResponseMessage();
        Query query = new Query(Criteria.where("userName").is(userName));
        Update update = new Update();
        update.set("password", password);

        WriteResult result = mongoTemplate.updateFirst(query, update, User.class);

        if (result != null) {
            bhmResponseMessage.setSuccess(true);
        } else {
            bhmResponseMessage.setSuccess(false);
        }
        return bhmResponseMessage;

    }

    @Override
    public BHMResponseMessage updateActive(String userName, boolean isActive, String collectionName) {
        getBhmResponseMessage();
        return bhmResponseMessage;
    }

    @Override
    public BHMResponseMessage createUser(AuthUser authUser, String collectionName) {
        getBhmResponseMessage();
        try {
            mongoTemplate.save(authUser, collectionName);
            logger.info("bhmResponseMessage code: "+bhmResponseMessage.hashCode());
            bhmResponseMessage.setSuccess(true);
        } catch (Exception e) {
            bhmResponseMessage.setSuccess(false);
            bhmResponseMessage.setResponseMessage(authUser.getUserName() + " Creation" + FAIL);
            bhmResponseMessage.setRootCause(e.getMessage());
            logger.info("bhmResponseMessage code 1: "+bhmResponseMessage.hashCode());
            logger.error(e.getMessage());
        }
        if (bhmResponseMessage.isSuccess()) {
            bhmResponseMessage.setResponseMessage(authUser.getUserName() + " Creation" + SUCCESS);
        }
        return bhmResponseMessage;

    }
}
