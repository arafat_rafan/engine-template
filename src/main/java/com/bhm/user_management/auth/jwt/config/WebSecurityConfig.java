package com.bhm.user_management.auth.jwt.config;

import com.bhm.user_management.auth.jwt.filter.JWTAuthenticationFilter;
import com.bhm.user_management.auth.jwt.service.TokenAuthenticationService;
import com.bhm.user_management.auth.jwt.common.RestEndpointExceptionHandler;
import com.bhm.user_management.auth.service.BHMUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import static com.bhm.user_management.auth.common.AuthConstant.SIGN_UP_URL;
import static com.bhm.user_management.auth.common.AuthConstant.LOGIN_URL;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final Logger logger = LoggerFactory.getLogger(WebSecurityConfig.class);

    @Autowired
    private BHMUserDetailsService bhmUserDetailsService;

    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @Autowired
    RestEndpointExceptionHandler restEndpointExceptionHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable();

        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
                 .antMatchers(HttpMethod.POST, LOGIN_URL).permitAll()
                .anyRequest().authenticated()
                .and()
                // filter the api/login requests
               /* .addFilterBefore(new JWTLoginFilter(authenticationManager(), tokenAuthenticationService, passwordEncoder),
                        BasicAuthenticationFilter.class)*/
                // And filter other requests to check the presence of JWT in header
                .addFilterBefore(new JWTAuthenticationFilter(tokenAuthenticationService),
                        BasicAuthenticationFilter.class)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint(restEndpointExceptionHandler);
    }

    /**
     * Add those endpoint need to ignore while requesting
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.POST, "/login");
    }
}