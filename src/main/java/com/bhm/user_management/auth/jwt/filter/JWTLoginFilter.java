package com.bhm.user_management.auth.jwt.filter;

import com.bhm.user_management.auth.jwt.service.TokenAuthenticationService;
import com.bhm.user_management.auth.model.AuthUser;
import com.bhm.user_management.auth.model.AuthUser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


import static java.util.Collections.emptyList;

public class JWTLoginFilter extends UsernamePasswordAuthenticationFilter {

    private final Logger logger = LoggerFactory.getLogger(JWTLoginFilter.class);
    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;
    @Autowired
    PasswordEncoder passwordEncoder;

    public JWTLoginFilter(AuthenticationManager authManager, TokenAuthenticationService tokenAuthenticationService, PasswordEncoder passwordEncoder) {
        setAuthenticationManager(authManager);
        this.tokenAuthenticationService = tokenAuthenticationService;
        this.passwordEncoder = passwordEncoder;

    }

    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException {
        try {
            logger.info("attemptAuthentication "+req.toString());
            AuthUser authUser = new ObjectMapper()
                    .readValue(req.getInputStream(), AuthUser.class);;
            authUser.setPassword(passwordEncoder.encode(authUser.getPassword()));
            logger.info("attemptAuthentication authUser "+authUser.toString());
            return getAuthenticationManager().authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authUser.getUserName(),
                            authUser.getPassword(),
                            emptyList()
                    )
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(
            HttpServletRequest req,
            HttpServletResponse res, FilterChain chain,
            Authentication auth) throws IOException, ServletException {
        logger.info("successfulAuthentication "+req.toString());
        tokenAuthenticationService
                .addAuthentication(res, auth.getName());
    }
}