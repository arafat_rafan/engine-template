package com.bhm.user_management.user.controller;

import com.bhm.common.ResponseMessage;
import com.bhm.user_management.auth.jwt.service.TokenAuthenticationService;
import com.bhm.user_management.auth.model.AuthUser;
import com.bhm.user_management.auth.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 7/17/17
 */
@RestController
@RequestMapping("/bhm/user/")
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private AuthService authService;

    @GetMapping("users")
    public ResponseMessage getAllUsers(){
        return authService.getAllUsers().join();
    }
}
