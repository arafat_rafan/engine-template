package com.bhm.user_management.auth.jwt.service;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security
        .authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

import static java.util.Collections.emptyList;
import static com.bhm.user_management.auth.common.AuthConstant.HEADER_STRING;
import static com.bhm.user_management.auth.common.AuthConstant.TOKEN_PREFIX;
import static com.bhm.user_management.auth.common.AuthConstant.SECRET;

@Service
public class TokenAuthenticationService {
    private final Logger logger = LoggerFactory.getLogger(TokenAuthenticationService.class);

    final int VALIDATIONTIME = 180000000; // 1 minute

    public void addAuthentication(HttpServletResponse res, String username) {
        logger.info(res.toString());
        long expireTime = System.currentTimeMillis() + VALIDATIONTIME;
        logger.info("addAuthentication "+expireTime);
        String JWT = Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(expireTime))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
    }

    public Authentication getAuthentication(HttpServletRequest request) {
        logger.info(request.toString());
        String token = request.getHeader(HEADER_STRING);
        if (token != null && token.startsWith(TOKEN_PREFIX)) {
            // parse the token.
            try {
                String user = Jwts.parser()
                        .setSigningKey(SECRET)
                        .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                        .getBody()
                        .getSubject();

                return user != null ?
                        new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
                        null;
            } catch (ExpiredJwtException e) {
               logger.error(e.getMessage());
            } catch (UnsupportedJwtException e) {
                logger.error(e.getMessage());
            } catch (MalformedJwtException e) {
                logger.error(e.getMessage());
            } catch (SignatureException e) {
                logger.error(e.getMessage());
            } catch (IllegalArgumentException e) {
                logger.error(e.getMessage());
            }
        }
        return null;
    }
}