package com.bhm.common;

import com.bhm.user_management.auth.model.AuthUser;

import java.util.List;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 1/30/18
 */
public interface ResponseMessage {
    boolean isSuccess();
    String getResponseMessage();
    public String getRootCause();
    public List<AuthUser> getData();
}
