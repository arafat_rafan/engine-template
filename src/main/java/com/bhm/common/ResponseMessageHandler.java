package com.bhm.common;

import org.springframework.stereotype.Component;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 2/5/18
 */
@Component
public class ResponseMessageHandler {

    public ResponseMessage setInformation(boolean isSuccess, String rMessage, String rCause) {
        BHMResponseMessage bhmResponseMessage = new BHMResponseMessage();
        bhmResponseMessage.setSuccess(isSuccess);
        bhmResponseMessage.setResponseMessage(rMessage);
        bhmResponseMessage.setRootCause(rCause);
        return bhmResponseMessage;
    }
}
