package com.bhm.user_management.auth.service;

import com.bhm.user_management.auth.jwt.filter.JWTLoginFilter;
import com.bhm.user_management.auth.model.UserCredential;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 1/16/18
 */

@Service
public class BHMUserDetailsService implements UserDetailsService {

    private final Logger logger = LoggerFactory.getLogger(BHMUserDetailsService.class);

    @Autowired
    AuthService authService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserCredential userCredential = authService.findUser(username).join();

        if(!userCredential.isActive()){
            userCredential = null;
        }
        if (userCredential == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            return new User(userCredential.getUserName(), userCredential.getPassword(), Collections.emptyList());
        }
    }

}
