package com.bhm.user_management.auth.model;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 1/15/18
 */
public interface UserCredential {
    String getUserName();
    String getPassword();
    boolean isActive();
}
