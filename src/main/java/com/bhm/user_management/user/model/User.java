package com.bhm.user_management.user.model;

import com.bhm.user_management.auth.model.AuthUser;
import com.bhm.user_management.auth.model.UserCredential;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 7/17/17
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "user")
public class User extends AuthUser {

    private String id;
    /**
     * @param lastLoginAttempt
     */
    private Timestamp lastLoginTimestamp;
    /**
     * lastLoginAttempt represents last five failed login attempt from IP Address(s)
     */
    private String lastLoginAttempt;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Timestamp getLastLoginTimestamp() {
        return lastLoginTimestamp;
    }

    public void setLastLoginTimestamp(Timestamp lastLoginTimestamp) {
        this.lastLoginTimestamp = lastLoginTimestamp;
    }

    public String getLastLoginAttempt() {
        return lastLoginAttempt;
    }

    public void setLastLoginAttempt(String lastLoginAttempt) {
        this.lastLoginAttempt = lastLoginAttempt;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", lastLoginTimestamp=" + lastLoginTimestamp +
                ", lastLoginAttempt='" + lastLoginAttempt + '\'' +
                '}';
    }
}
