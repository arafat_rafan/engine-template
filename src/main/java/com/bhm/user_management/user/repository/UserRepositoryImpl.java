package com.bhm.user_management.user.repository;

import com.bhm.common.BHMResponseMessage;
import com.bhm.user_management.auth.model.AuthUser;
import com.bhm.user_management.user.model.User;
import com.mongodb.WriteResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import static com.bhm.user_management.auth.common.AuthConstant.FAIL;
import static com.bhm.user_management.auth.common.AuthConstant.SUCCESS;

/**
 * @author : arafat
 * @version : 1.0v.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 7/17/17
 */

@Repository
public class UserRepositoryImpl implements UserRepositoryCustom {

    private final Logger logger = LoggerFactory.getLogger(UserRepositoryImpl.class);

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    BHMResponseMessage bhmResponseMessage;
}
