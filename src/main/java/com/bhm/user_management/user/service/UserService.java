package com.bhm.user_management.user.service;

import com.bhm.common.BHMResponseMessage;
import com.bhm.common.ResponseMessage;
import com.bhm.user_management.auth.model.AuthUser;
import com.bhm.user_management.auth.repository.AuthRepository;
import com.bhm.user_management.user.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

import static com.bhm.user_management.auth.common.AuthConstant.*;


/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 7/17/17
 */
@Service
public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    private UserRepository userRepository;



    @Async("userManagementThreadPoolExecutor")
    public CompletableFuture<ResponseMessage> getAllUsers() {
        BHMResponseMessage responseMessage = new BHMResponseMessage();
        responseMessage.setData(userRepository.findAllByIsActive(true));
        return CompletableFuture.completedFuture(responseMessage);
    }

}
