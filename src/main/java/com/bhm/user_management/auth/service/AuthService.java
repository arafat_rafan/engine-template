package com.bhm.user_management.auth.service;

import com.bhm.common.BHMResponseMessage;
import com.bhm.common.ResponseMessage;
import com.bhm.common.ResponseMessageHandler;
import com.bhm.user_management.auth.model.AuthUser;
import com.bhm.user_management.auth.repository.AuthRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

import static com.bhm.user_management.auth.common.AuthConstant.*;


/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 7/17/17
 */
@Service
public class AuthService {

    private final Logger logger = LoggerFactory.getLogger(AuthService.class);
    @Autowired
    private AuthRepository authRepository;
    @Autowired
    private ResponseMessageHandler responseMessageHandler;

    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Async("userManagementThreadPoolExecutor")
    public CompletableFuture<AuthUser> findUser(String userName) {

        logger.info("findUser userName " + userName);
        AuthUser authUser = authRepository.findByUserName(userName);
        logger.info("findUser loginUserCredential " + authUser);
        return CompletableFuture.completedFuture(authUser);
    }

    @Async("userManagementThreadPoolExecutor")
    public CompletableFuture<ResponseMessage> login(AuthUser authUser) {

        AuthUser loginUser = findUser(authUser.getUserName()).join();
        ResponseMessage responseMessage;
        if (loginUser == null) {
            responseMessage = responseMessageHandler.setInformation(false, authUser.getUserName() + " login" + FAIL, authUser.getUserName() + " Not Found");
        } else {
            logger.info("login loginUser " + loginUser.toString());
            logger.info("login passwordEncoder " + passwordEncoder.matches(authUser.getPassword(), loginUser.getPassword()));
            if (!loginUser.isActive()) {
                responseMessage = responseMessageHandler.setInformation(false, loginUser.getUserName() + " login" + FAIL, loginUser.getUserName() + " Not an " + ACTIVE + USER);
            } else if (passwordEncoder.matches(authUser.getPassword(), loginUser.getPassword())) {
                responseMessage = responseMessageHandler.setInformation(true, loginUser.getUserName() + " login" + SUCCESS, null);
            } else {
                responseMessage = responseMessageHandler.setInformation(false, loginUser.getUserName() + " login" + FAIL, "Wrong Password");
            }
        }
        return CompletableFuture.completedFuture(responseMessage);
    }

    @Async("userManagementThreadPoolExecutor")
    public CompletableFuture<ResponseMessage> createUser(AuthUser authUser) {
        logger.info("createUser " + authUser.toString());
        authUser.setPassword(passwordEncoder.encode(authUser.getPassword()));
        return CompletableFuture.completedFuture(authRepository.createUser(authUser, "user"));
    }

    @Async("userManagementThreadPoolExecutor")
    public CompletableFuture<ResponseMessage> getAllUsers() {
        BHMResponseMessage responseMessage = new BHMResponseMessage();
        responseMessage.setData(authRepository.findAllByIsActive(true));
        return CompletableFuture.completedFuture(responseMessage);
    }
}
