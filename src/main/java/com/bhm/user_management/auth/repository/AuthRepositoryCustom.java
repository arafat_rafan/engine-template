package com.bhm.user_management.auth.repository;

import com.bhm.common.BHMResponseMessage;
import com.bhm.user_management.auth.model.AuthUser;
import com.bhm.user_management.user.model.User;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 7/17/17
 */
public interface AuthRepositoryCustom {

    BHMResponseMessage updatePassword(String userName, String password, String collectionName);

    BHMResponseMessage updateActive(String userName, boolean isActive, String collectionName);

    BHMResponseMessage createUser(AuthUser authUser, String collectionName);
}
