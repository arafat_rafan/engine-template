package com.bhm.user_management.user.repository;

import com.bhm.user_management.auth.model.AuthUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : bhm
 * @since : 7/18/17
 */
@Repository
public interface UserRepository extends MongoRepository<AuthUser, String> , UserRepositoryCustom {
    List<AuthUser> findAllByIsActive(boolean isActive);
}
